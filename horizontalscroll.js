$(function () {
    //Seta o tamanho do navigator
    var maxWidth = 0;
    $(".navigator-item").each(function(i, e){maxWidth += $(e).width();});
    $(".navigator-container").width(maxWidth);

    //Pega o primeiro item e aplica a classe active
    $(".navigator-item:first-of-type").addClass('active');
    scrollToElement(".navigator-item.active");

    function scrollToElement() {
        var scl = $(".navigator-item.active").offset().left;
        $("body, html").stop().animate({scrollLeft: scl}, 1000);
    }
    function addNext() {
        $(".navigator-item.active").prev().addClass('active');
        $(".navigator-item.active").next().removeClass('active');
    }
    function addPrev() {
        $(".navigator-item.active").next().addClass('active');
        $(".navigator-item.active").prev().removeClass('active');
    }

    $('body').bind('mousewheel DOMMouseScroll', function (e) {
        e.preventDefault();

        if (e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
            //Scrolling UP
            addNext();
            scrollToElement();
        } else {
            //Scrolling DOWN
            addPrev();
            scrollToElement();
        }
    });

    //Controla os botões próximo e anterior
    $(".prev").on('click', function () {
        addNext();
        scrollToElement();
    });
    $(".next").on('click', function () {
        addPrev();
        scrollToElement();
    });

    //Controle das setas
    $(document).on('keydown', function(e){
    	if(e.keyCode === 39){
    		addNext();
        	scrollToElement();
    	}
    	if(e.keyCode === 37){
    		addPrev();
        	scrollToElement();
    	}
    });
    
});
