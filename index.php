<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Horizontal Scroll Testes</title>
        <link rel='stylesheet' href="style.css"/>

    </head>
    <body>

        <div class="navigator-container">
        
            <div class="navigator-item">
                a
            </div>
        
            <div class="navigator-item">
                b
            </div>
            
        </div>
        
        <div class="navigator-controller">
            <div class="next">Próximo</div>
            <div class="prev">Anterior</div>
        </div>
      
        <!--Chamada dos scripts-->
        <script src="jquery-3.3.1.min.js"></script>
        <script src="horizontalscroll.js"></script>
    </body>
</html>
